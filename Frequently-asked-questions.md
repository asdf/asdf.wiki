### How can I tell which version of ASDF I am running?

This is already a [FAQ item in the official manual](https://common-lisp.net/project/asdf/asdf/How-do-I-detect-the-ASDF-version_003f.html) but shall be repeated here: 

On recent versions of ASDF, you can simply use
```common-lisp
(asdf:asdf-version)
```
The following piece of code will also work on much older versions, though.
```common-lisp
(when (find-package :asdf)
  (let ((ver (symbol-value
                   (or (find-symbol (string :*asdf-version*) :asdf)
                       (find-symbol (string :*asdf-revision*) :asdf)))))
    (etypecase ver
      (string ver)
      (cons (with-output-to-string (s)
              (loop for (n . m) on ver
                    do (princ n s)
                       (when m (princ "." s)))))
      (null "1.0"))))
```

### My favourite lisp implementation uses an old version of ASDF. What can I do?

If your current working directory is main directory of an ASDF clone, please first run
```shell
git checkout release
make ext
```
to switch to the highly recommended `release` branch and fetch the necessary submodules. 

If you quickly mean to test if the old version of ASDF (and thus UIOP) your lisp implementation is shipping is what's causing your troubles, you can simple run
```shell
make
```
to produce a build/asdf.lisp which you can then use in ways like this: `sbcl --load build/asdf.lisp --load ~/code-that-was-causing-errors.lisp`

The proper solution for permanently upgrading the bundled ASDF version of your lisp implementation, however, would be, depending on which one you're on, something like
```shell
ccl --load ./tools/asdf-tools -- install-asdf ccl # note the double-dash
ecl --load ./tools/asdf-tools -- install-asdf ecl # note the double-dash
sbcl --load ./tools/asdf-tools install-asdf sbcl
```

The fact that your implementation occurs twice in each call is not by accident: The first is used to compile the lisp-code that makes up install-asdf; the second is the target you're upgrading. They need not be identical; in particular, you could use sbcl to upgrade cmucl's asdf via
```shell
sbcl --load ./tools/asdf-tools install-asdf cmucl
```
which comes in handy now that building/running asdf-tools via cmucl itself is [broken](https://bugs.launchpad.net/asdf/+bug/1682625).

### How do I build an executable with ASDF-3?

[cl-echo](https://github.com/epipping/cl-echo) is a sample project created to show-case basic ASDF-3 features like `asdf:program-op`.
