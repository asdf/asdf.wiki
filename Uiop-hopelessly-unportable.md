The following features currently cannot be expected to make their way into UIOP because vendor support is lacking.

# `uiop:process-status` (as an upgrade from `%process-status`)

The lack a generally working `uiop:process-status` was already used to motivate the addition of the two functions, namely [`uiop:process-alive-p` and `uiop:wait-process`](https://gitlab.common-lisp.net/asdf/asdf/wikis/UIOP-Relatively-Well-Supported#uiopprocess-alive-p-and-uiopwait-process).

We snippets of code we've already used there we'll simply extend to make our point here: By going through the sequence of sending SIGSTOP, SIGCONT, SIGTSTP, and SIGCONT again, culminating in a SIGTERM, we obtain the following output (the comparison with a regular exit is omitted):

- The output from running the test on a few implementations 

```
### abcl-1.5.0-fasl43-linux-x64
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
            T | NOT-IMPLEMENTED
     NIL, 143 | NOT-IMPLEMENTED
     NIL, 143 | NOT-IMPLEMENTED
### acl-10.1-linux-x86
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
 NIL, 143, 15 | SIGNALED, 15
 NIL, 143, 15 | SIGNALED, 15
### ccl-1.11-f96-linux-x86
            T | RUNNING
            T | RUNNING
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 20
            T | STOPPED, 20
            T | STOPPED, 20
            T | STOPPED, 20
            T | STOPPED, 20
            T | STOPPED, 20
            T | STOPPED, 20
            T | STOPPED, 20
 NIL, 143, 15 | SIGNALED, 15
 NIL, 143, 15 | SIGNALED, 15
### cmu-20c_release-20c__20c_unicode_-linux-x86
            T | RUNNING
            T | RUNNING
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
 NIL, 143, 15 | SIGNALED, 15
 NIL, 143, 15 | SIGNALED, 15
### cmu-21b__21b_unicode_-linux-x86
            T | RUNNING
            T | RUNNING
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
            T | STOPPED
 NIL, 143, 15 | SIGNALED, 15
 NIL, 143, 15 | SIGNALED, 15
### cmu-snapshot-2017-04-42-g7f3040a7e__21b_unicode_-linux-x86
            T | RUNNING
            T | RUNNING
            T | STOPPED
            T | STOPPED
            T | CONTINUED
            T | CONTINUED
            T | STOPPED
            T | STOPPED
            T | CONTINUED
            T | CONTINUED
            T | CONTINUED
            T | CONTINUED
            T | CONTINUED
            T | CONTINUED
            T | CONTINUED
            T | CONTINUED
            T | CONTINUED
            T | CONTINUED
 NIL, 143, 15 | SIGNALED, 15
 NIL, 143, 15 | SIGNALED, 15
### ecl-16.1.3-linux-x64
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
            T | RUNNING
 NIL, 143, 15 | SIGNALED, 15
 NIL, 143, 15 | SIGNALED, 15
### ecl-16.1.3-f4feda8a-linux-x64
            T | RUNNING
            T | RUNNING
            T | STOPPED, 19
            T | STOPPED, 19
            T | RESUMED, 18
            T | RESUMED, 18
            T | STOPPED, 19
            T | STOPPED, 19
            T | RESUMED, 18
            T | RESUMED, 18
            T | STOPPED, 20
            T | STOPPED, 20
            T | RESUMED, 18
            T | RESUMED, 18
            T | STOPPED, 20
            T | STOPPED, 20
            T | RESUMED, 18
            T | RESUMED, 18
 NIL, 143, 15 | SIGNALED, 15
 NIL, 143, 15 | SIGNALED, 15
### mkcl-1.1.10.19-2dbfa99-linux-x64
            T | RUNNING
            T | RUNNING
            T | STOPPED
            T | STOPPED
            T | RUNNING
            T | RUNNING
            T | STOPPED
            T | STOPPED
            T | RUNNING
            T | RUNNING
            T | STOPPED
            T | STOPPED
            T | RUNNING
            T | RUNNING
            T | STOPPED
            T | STOPPED
            T | RUNNING
            T | RUNNING
 NIL, 143, 15 | SIGNALED, 15
 NIL, 143, 15 | SIGNALED, 15
### sbcl-1.3.21-linux-x64
            T | RUNNING
            T | RUNNING
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
            T | STOPPED, 19
### sbcl-1.3.21.149-9e3453954-linux-x64
            T | RUNNING
            T | RUNNING
            T | STOPPED, 19
            T | STOPPED, 19
            T | CONTINUED, 18
            T | CONTINUED, 18
            T | STOPPED, 19
            T | STOPPED, 19
            T | CONTINUED, 18
            T | CONTINUED, 18
            T | STOPPED, 20
            T | STOPPED, 20
            T | CONTINUED, 18
            T | CONTINUED, 18
            T | STOPPED, 20
            T | STOPPED, 20
            T | CONTINUED, 18
            T | CONTINUED, 18
 NIL, 143, 15 | SIGNALED, 15
 NIL, 143, 15 | SIGNALED, 15
```

- The runner (process-status-test.sh ) that runs the underlying tests on a couple of implementations

```shell
a=$HOME/asdf/build/asdf.lisp
f=$HOME/lisp-scripts/process-status-test.lisp
b=$HOME/lisp-installs

### Loading asdf and requiring mk-unix: the fact that "system mk-unix is out of date" is treated
### as a fatal error prior to ab65958cc97addf88b756dcf135b9ace76a13f02.
# (
#     l=$b/mkcl-1.1.10/bin/mkcl;
#     $l -eval '(setf *load-verbose* nil)' -eval "(load \"$a\")" -eval "(load \"$f\")" -eval '(quit)'
# )

(
    l=$b/abcl-bin-1.5.0/abcl.jar
    java -jar $l --noinform --batch --load $a --load $f
)
(
    l=$b/acl10.1express-linux-x86/alisp
    $l --batch -e '(setf *load-verbose* nil)' -L $a -L $f --kill
)
(
    l=$b/ccl-1.11-linux/lx86cl
    $l --load $a --load $f --eval '(quit)'
)
(
    l=$b/cmucl-20c-x86-linux/bin/lisp
    $l -quiet -batch -eval "(handler-bind ((warning #'muffle-warning)) (load \"$a\"))" -load $f -eval '(quit)'
)
(
    l=$b/cmucl-21b-x86-linux/bin/lisp
    $l -quiet -batch -eval "(handler-bind ((warning #'muffle-warning)) (load \"$a\"))" -load $f -eval '(quit)'
)
(
    l=$b/cmucl-2017-04-42-g7f3040a7e-x86-linux/bin/lisp
    $l -quiet -batch -eval "(handler-bind ((warning #'muffle-warning)) (load \"$a\"))" -load $f -eval '(quit)'
)
(
    l=$b/ecl-16.1.3/bin/ecl
    $l --eval '(setf *load-verbose* nil)' --eval "(load \"$a\")" --eval "(load \"$f\")" --eval '(quit)'
)
(
    l=$b/ecl-git/bin/ecl
    $l --eval '(setf *load-verbose* nil)' --eval "(load \"$a\")" --eval "(load \"$f\")" --eval '(quit)'
)
(
    l=$b/mkcl-git/bin/mkcl;
    $l -eval '(setf *load-verbose* nil)' -eval "(load \"$a\")" -eval "(load \"$f\")" -eval '(quit)'
)
(
    l=$b/sbcl-1.3.21/bin/sbcl
    $l --noinform --non-interactive --load $a --load $f
)
(
    l=$b/sbcl-git/bin/sbcl
    $l --noinform --non-interactive --load $a --load $f
)
```

- The final script (process-status-test.lisp) with all the tests:

```common-lisp
(defmacro call-potentially-unimplemented-function (function &rest args)
  (let ((block-name (gensym "block")))
    `(block ,block-name
	    (handler-bind ((uiop/utility::not-implemented-error
			    #'(lambda (c)
				(declare (ignore c))
				(return-from ,block-name :not-implemented))))
			  (funcall ,function ,@args)))))

(defun analyse (process)
  (format t "~13@a | ~a~%"
	  (format nil "~{~a~^, ~}"
		  (let ((alive (uiop:process-alive-p process)))
		    (if alive
			(list T)
		      `(,alive ,@(multiple-value-list (uiop:wait-process process))))))
	  (format nil "~{~a~^, ~}"
		  (multiple-value-list
		   (call-potentially-unimplemented-function #'uiop/launch-program::%process-status process)))))

(defun send-signal-externally (process signal)
  (uiop:run-program (list "/bin/kill"
                          (format nil "-~a" signal)
                          (format nil "~a" (uiop:process-info-pid process)))))

(defun send->sleep->analyse->sleep->analyse (process &optional signal)
  (when signal
    (send-signal-externally process signal))
  (sleep 1)
  (analyse process)
  (sleep 1)
  (analyse process))

(defconstant +sigcont+
  #+abcl (java:jcall "getNumber" (java:jnew "sun.misc.Signal" "CONT"))
  #+allegro excl.osi:*sigcont*
  #+clozure (symbol-value (read-from-string "#$SIGCONT"))
  #+cmu unix:sigcont
  #+ecl ext:+sigcont+
  #+lispworks system::unix-sigcont
  #+mkcl (progn (require :mk-unix)
		(symbol-value (find-symbol (symbol-name :sigcont)
					   (find-package :mk-unix))))
  #+sbcl (progn (require :sb-posix)
                (symbol-value (find-symbol (symbol-name :sigcont)
                                           (find-package :sb-posix)))))
(defconstant +sigstop+
  #+abcl (java:jcall "getNumber" (java:jnew "sun.misc.Signal" "STOP"))
  #+allegro excl.osi:*sigstop*
  #+clozure (symbol-value (read-from-string "#$SIGSTOP"))
  #+cmu unix:sigstop
  #+ecl ext:+sigstop+
  #+lispworks system::unix-sigstop
  #+mkcl (progn (require :mk-unix)
		(symbol-value (find-symbol (symbol-name :sigstop)
					   (find-package :mk-unix))))
  #+sbcl (progn (require :sb-posix)
                (symbol-value (find-symbol (symbol-name :sigstop)
                                           (find-package :sb-posix)))))

(defconstant +sigtstp+
  #+abcl (java:jcall "getNumber" (java:jnew "sun.misc.Signal" "TSTP"))
  #+allegro excl.osi:*sigtstp*
  #+clozure (symbol-value (read-from-string "#$SIGTSTP"))
  #+cmu unix:sigtstp
  #+ecl ext:+sigtstp+
  #+lispworks system::unix-sigtstp
  #+mkcl (progn (require :mk-unix)
		(symbol-value (find-symbol (symbol-name :sigtstp)
					   (find-package :mk-unix))))
  #+sbcl (progn (require :sb-posix)
                (symbol-value (find-symbol (symbol-name :sigtstp)
                                           (find-package :sb-posix)))))

(defconstant +sigterm+
  #+abcl (java:jcall "getNumber" (java:jnew "sun.misc.Signal" "TERM"))
  #+allegro excl.osi:*sigterm*
  #+clozure (symbol-value (read-from-string "#$SIGTERM"))
  #+cmu unix:sigterm
  #+ecl ext:+sigterm+
  #+lispworks system::unix-sigterm
  #+mkcl (progn (require :mk-unix)
		(symbol-value (find-symbol (symbol-name :sigterm)
					   (find-package :mk-unix))))
  #+sbcl (progn (require :sb-posix)
                (symbol-value (find-symbol (symbol-name :sigterm)
                                           (find-package :sb-posix)))))

(progn
  (format t "### ~a~%" (asdf:implementation-identifier))

  (let ((p-sleep (uiop:launch-program '("/bin/sleep" "21"))))
    (send->sleep->analyse->sleep->analyse p-sleep)

    (send->sleep->analyse->sleep->analyse p-sleep +sigstop+)
    (send->sleep->analyse->sleep->analyse p-sleep +sigcont+)
    (send->sleep->analyse->sleep->analyse p-sleep +sigstop+)
    (send->sleep->analyse->sleep->analyse p-sleep +sigcont+)

    (send->sleep->analyse->sleep->analyse p-sleep +sigtstp+)
    (send->sleep->analyse->sleep->analyse p-sleep +sigcont+)
    (send->sleep->analyse->sleep->analyse p-sleep +sigtstp+)
    (send->sleep->analyse->sleep->analyse p-sleep +sigcont+)

    (send->sleep->analyse->sleep->analyse p-sleep +sigterm+))

  (uiop:quit 0))
```

Note: Output of this code was captured on a machine where !83 had already been merged into master.

There are a few bugs related to these issues, namely:
- Clozure CL: [external-process-status not always updated correctly](https://trac.clozure.com/ccl/ticket/1375) (`%process-status` will report bogus)
- CMU CL: [Report proper process status](https://gitlab.common-lisp.net/cmucl/cmucl/issues/41)
- ECL: [Sleeping process not reported as sleeping](https://gitlab.com/embeddable-common-lisp/ecl/issues/273) (`%process-status` will report bogus) **(fixed on the *develop* branch; expected in the next release after 16.1.3)**
- SBCL: [Sleeping processes are marked as sleeping forever ](https://sourceforge.net/p/sbcl/mailman/message/35259260) (`%process-status` will report bogus) **(fixed on the *master* branch; expected in the next release after 1.3.21)**
- SBCL: [zombie processes handled incorrectly](https://bugs.launchpad.net/sbcl/+bug/1624941) (`%process-status` will report bogus; `wait-process` will hang) **(fixed on the *master* branch; expected in the next release after 1.3.21)**

# Proper symlink handling

Related bugs:
- [[LispWorks] UIOP:DIRECTORY* follows symbolic links](https://bugs.launchpad.net/asdf/+bug/1205653)
- [ASDF on CCL does not follow symlinks in registry](https://bugs.launchpad.net/asdf/+bug/1205555)

Related (necessarily incomplete) merge requests:
- [WIP: Handle symbolic links ](https://gitlab.common-lisp.net/asdf/asdf/merge_requests/27)

Another related snippet with sample code:
- [UIOP and symlinks](https://gitlab.common-lisp.net/epipping/asdf/wikis/uiop-and-symlinks)

# `:environment` support in `uiop:run-program`

Related feature request:
- [Feature request: keyword `:environment` for `uiop:run-program`](https://bugs.launchpad.net/asdf/+bug/1636903)

Overview of a few superficial differences:
- ABCL: `:environment` (alist), `:clear-environment`
- Allegro CL: `:environment` (alist or list)
- CMU CL: `:env` (alist)
- Clozure CL: `:env` (alist)
- ECL: `:environ` (list/keyword) **([inconsistent behaviour](https://gitlab.com/embeddable-common-lisp/ecl/issues/298) now [fixed via new `:default` keyword](https://gitlab.com/embeddable-common-lisp/ecl/commit/7db45430514812ee56874191c143597d9935c449) on the *develop* branch; expected in the next release after 16.1.3)**
- LispWorks: `:environment` (weird alist)
- MKCL: **ignored**: `:environment`
- SBCL: `:environment` (list)
- SCL: `:env` (alist)

# `:pty` support in `uiop:run-program`

Related feature request:
- [Support for pty](https://github.com/sellout/external-program/issues/37)

# `:status-hook` support in `uiop:run-program`

Issues:
- Only available on CMU CL, Clozure CL, SBCL
- Lambda expects a process rather than a `process-info` class object

# Support for streams without file handles in `uiop:run-program`

Related feature request:
- ECL: [Feature request: Support streams without handles in run-program](https://gitlab.com/embeddable-common-lisp/ecl/issues/272)

# `uiop:posix-send-signal`

For now, we need to use the internal function `uiop:%posix-send-signal` which is not available everywhere. And it seems like a [bad idea](https://gitlab.com/embeddable-common-lisp/ecl/issues/269#note_39309014), too.

Bugs:
- ECL: [Feature request: Function `signal-process`](https://gitlab.com/embeddable-common-lisp/ecl/issues/269) **(closed as wontfix)**